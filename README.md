# SQA-CB-API Stress Tests

## Prerequisites

- Download and install `Python 3`.
- Clone the [repository](https://bitbucket.org/ebsproject/sqa-cb-api-stress-tests/src/develop/)
- Setup a virtual environment using [`virtualenv`](https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/) package
- Activate virtual environment and install required dependencies.
- Install Locust 
```bash
    sudo pip install locust
```

## Contents of this repository
```bash  
.
├── common
│   ├── __init__.py
│   ├── config.py     # contains host and token
│   └── master.conf   # contains configs for headless execution
├── env
├── locustfiles
│   ├── experiments.py  # sample locust test file for experiment
│   └── locustfile.py   # default or main locust test file
├── log
│   ├── example_exceptions.csv     # sample log for exceptions
│   ├── example_failures.csv       # sample log for failures
│   ├── example_stats_history.csv  # sample log for stats history
│   └── example_stats.csv          # sample log for stats
└── requirements.txt         # contains list of required packages
```

## Configuration
- common/config.py
```
host = 'https://cbapi-dev.ebsproject.org'
token = 'Bearer <token_str>'
# port = 5432
# username: postgres
# password: XXXX
```

- common/master.conf
```
locustfile = locustfiles/locustfile.py
host = https://cbapi-dev.ebsproject.org
users = 1
spawn-rate = 10
headless = true
master = false
expect-workers = 1
run-time = 1m
csv = log/example
```

## Execution
- Normal state - running with the web UI
```
locust -f locustfiles/locustfile.py --users=1 --spawn-rate=10 --csv=log/example
```

- Headless state - running without the web UI
```
locust -f locustfiles/locustfile.py --users=1 --spawn-rate=10 --run-time=1m --headless --csv=log/example 
```

- Custom state (set and call parameters from a master config file)
```
locust --config=common/master.conf

locustfile = locustfiles/locustfile.py
headless = true
master = false
expect-workers = 1
host = https://cbapi-dev.ebsproject.org
users = 1
spawn-rate = 10
run-time = 1m
csv = log/example
```