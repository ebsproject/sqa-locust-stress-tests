#!/bin/bash

CB_API_HOST=https://cbapi-dev.ebsproject.org
CB_API_TOKEN=xxxx

rm common/config.py

echo "host = '$CB_API_HOST'" >> common/config.py
echo "token = 'Bearer $CB_API_TOKEN'" >> common/config.py

docker-compose up
