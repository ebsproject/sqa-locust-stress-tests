from locust import HttpUser, between, events
import logging
import common.config as conf

from experiments import ExperimentsTest

import locust.stats
locust.stats.CSV_STATS_INTERVAL_SEC = 5 # default is 1 second
locust.stats.CSV_STATS_FLUSH_INTERVAL_SEC = 60 # Determines how often the data is flushed to disk, default is 10 seconds

class Main(HttpUser):
    
    wait_time = between(1, 5)
    host = conf.host
    tasks = {ExperimentsTest}

    @events.test_start.add_listener
    def on_test_start(environment, **kwargs):
        print("A new test is starting...")

    def on_start(self):
        token = conf.token
        response = self.client.headers = {'Authorization': token}

    @events.test_stop.add_listener
    def on_test_stop(environment, **kwargs):
        print("A new test is ending...")

    @events.quitting.add_listener
    def _(environment, **kw):
        if environment.stats.total.fail_ratio > 0.01:
            logging.error("Test failed due to failure ratio > 1%")
            environment.process_exit_code = 1
        elif environment.stats.total.avg_response_time > 200:
            logging.error("Test failed due to average response time ratio > 200 ms")
            environment.process_exit_code = 2
        elif environment.stats.total.get_response_time_percentile(0.95) > 800:
            logging.error("Test failed due to 95th percentile response time > 800 ms")
            environment.process_exit_code = 3
        else:
            environment.process_exit_code = 0