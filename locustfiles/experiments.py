from locust import TaskSet, task, SequentialTaskSet

class ExperimentsTest(SequentialTaskSet):

    # Initialize variables for experiment creation
    program_id = ''
    stage_id = ''
    season_id = ''
    person_id = ''
    item_id = ''
    crop_id = ''

    def get_logs(self, request, response):
        print (response.status_code)
        if (response.status_code == 401):
            response.failure('Unauthorized token error')
            # print (request, response.json()['metadata']['status'][0])
        elif (response.status_code == 200):
            response.success()
            # print (request, response.json()['metadata']['status'][0])
        else:
            response.failure('Unknown error')
            # print (request, response.json()['metadata']['status'][0])
    
    @task()
    def get_program_id(self):
        program_code = "IRSEA"
        endpoint = "/v3/programs/?programCode="+program_code

        with self.client.get(endpoint, catch_response=True) as response:
            self.get_logs(endpoint, response)
            if response.status_code == 200:
                self.program_id = response.json()['result']['data'][0]['programDbId']
                print ('PROGRAM_ID',self.program_id)

    @task()
    def post_stages_search(self):
        endpoint = "/v3/stages-search"
        params = {"stageCode":"AYT"}

        with self.client.post(endpoint, json=params, catch_response=True) as response:
            self.get_logs(endpoint, response)
            if response.status_code == 200:
                self.stage_id = response.json()['result']['data'][0]['stageDbId']
                print ('STAGE_ID',self.stage_id)

    @task()
    def post_seasons_search(self):
        endpoint = "/v3/seasons-search"
        params = {"seasonCode":"DS"}

        with self.client.post(endpoint, json=params, catch_response=True) as response:
            self.get_logs(endpoint, response)
            if response.status_code == 200:
                self.season_id = response.json()['result']['data'][0]['seasonDbId']
                print ("SEASON_ID", self.season_id)

    @task()
    def post_persons_search(self):
        endpoint = "/v3/persons-search"
        params = {"username":"nicola.costa"}

        with self.client.post(endpoint, json=params, catch_response=True) as response:
            self.get_logs(endpoint, response)
            if response.status_code == 200:
                self.person_id = response.json()['result']['data'][0]['personDbId']
                print ("PERSON_ID", self.person_id)

    @task()
    def post_items_search(self):
        endpoint = "/v3/items-search"
        params = {"abbrev":"BREEDING_TRIAL_DATA_PROCESS"}

        with self.client.post(endpoint, json=params, catch_response=True) as response:
            self.get_logs(endpoint, response)
            if response.status_code == 200:
                self.item_id = response.json()['result']['data'][0]['itemDbId']
                print ("ITEM_ID", self.item_id)

    @task()
    def post_crops_search(self):
        endpoint = "/v3/crops-search"
        params = {"cropCode":"RICE"}

        with self.client.post(endpoint, json=params, catch_response=True) as response:
            self.get_logs(endpoint, response)
            if response.status_code == 200:
                self.crop_id = str(response.json()['result']['data'][0]['cropDbId'])
                print ("CROP_ID", self.crop_id)

    @task()
    def create_experiment(self):
        print ('GLOBAL PROGRAM ID', self.program_id)
        print ('GLOBAL STAGE_ID', self.stage_id)
        print ('GLOBAL SEASON_ID', self.season_id)
        print ('GLOBAL PERSON_ID', self.person_id)
        print ('GLOBAL ITEM_ID', self.item_id)
        print ('GLOBAL CROP_ID', self.crop_id)

        endpoint = "/v3/experiments/"
        params = {
            "records": [
                {
                "programDbId": str(self.program_id),
                "stageDbId": str(self.stage_id),
                "experimentYear": "2020",
                "seasonDbId": str(self.season_id),
                "experimentName": "LOCUST TEST 1",
                "experimentType": "Breeding Trial",
                "experimentStatus": "draft",
                "stewardDbId": str(self.person_id),
                "dataProcessDbId": str(self.item_id),
                "cropDbId": str(self.crop_id)
                }
            ]
        }

        with self.client.post(endpoint, json=params, catch_response=True) as response:
            self.get_logs(endpoint, response)
            print (response)