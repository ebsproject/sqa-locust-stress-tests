FROM python:3.9.7

LABEL MAINTAINER=j.lagare

WORKDIR /locust

COPY . .

RUN pip install locust && chmod 777 -R locustfiles

ENTRYPOINT ["locust"]
